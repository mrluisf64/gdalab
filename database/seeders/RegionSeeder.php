<?php

namespace Database\Seeders;

use App\Models\Region;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::truncate();


        Region::create([
            'description' => 'EUR',
            'status' => 'A'
        ]);

        Region::create([
            'description' => 'ASIA',
            'status' => 'A'
        ]);

        Region::create([
            'description' => 'AMERICA',
            'status' => 'A'
        ]);

        Region::create([
            'description' => 'EEUU',
            'status' => 'I'
        ]);

        Region::create([
            'description' => 'RUSIA',
            'status' => 'trash'
        ]);

        Region::create([
            'description' => 'AFRICA',
            'status' => 'A'
        ]);
    }
}
