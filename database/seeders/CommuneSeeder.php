<?php

namespace Database\Seeders;

use App\Models\Commune;
use App\Models\Region;
use Illuminate\Database\Seeder;

class CommuneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Commune::truncate();
        $region1 = Region::find(1);
        $region2 = Region::find(2);
        $region3 = Region::find(3);

        $region1->communes()->createMany([
            [
                'description' => 'AMR',
                'status' => 'A',
                'id_reg' => $region1->id_reg
            ],
            [
                'description' => 'ERR',
                'status' => 'I',
                'id_reg' => $region1->id_reg
            ]
        ]);

        $region2->communes()->createMany([
            [
                'description' => 'OBJ',
                'status' => 'A',
                'id_reg' => $region2->id_reg
            ],
            [
                'description' => 'YEN',
                'status' => 'trash',
                'id_reg' => $region2->id_reg
            ]
        ]);

        $region3->communes()->createMany([
            [
                'description' => 'ERR',
                'status' => 'I',
                'id_reg' => $region3->id_reg
            ],
            [
                'description' => 'MEH',
                'status' => 'A',
                'id_reg' => $region3->id_reg
            ],
            [
                'description' => 'IUW',
                'status' => 'A',
                'id_reg' => $region3->id_reg
            ]
        ]);
    }
}
