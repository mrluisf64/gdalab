<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->string('dni')->unique();
            $table->integer('id_reg');
            $table->integer('id_com');
            $table->string('email')->unique()->comment('Correo Electrónico');
            $table->string('name')->comment('Nombre');
            $table->string('last_name')->comment('Apellido');
            $table->string('address')->nullable()->comment('Dirección');
            $table->date('date_reg')->comment('Fecha y hora del registro');
            $table->enum('status', ['A', 'I', 'trash'])->comment('estado del registro:\nA
            : Activo\nI : Desactivo\ntrash : Registro eliminado',);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
