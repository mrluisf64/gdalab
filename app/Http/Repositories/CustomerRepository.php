<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\CustomerInterface;
use App\Models\Commune;
use App\Models\Customer;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

class CustomerRepository implements CustomerInterface
{
    use ProvidesConvenienceMethods;

    public function getCustomerByEmail($value)
    {
        return Customer::with(['commune.region'])->where('email', $value)->first();
    }

    public function getCustomerByDni($value)
    {
        return Customer::with(['commune.region'])->where('dni', $value)->first();
    }

    public function showByScope($scope, $value)
    {
        $request = new Request([$scope => $value]);
        switch ($scope) {
            case 'email':
                return $this->searchCustomerByEmail($request, $value);
                break;
            case 'dni':
                return $this->searchCustomerByDni($request, $value);
                break;
        }
    }

    public function searchCustomerByEmail($request, $value)
    {
        $this->validate($request, [
            'email' => 'email|exists:customers,email'
        ]);

        $customer = $this->getCustomerByEmail($value);

        if ($customer->status == 'I' || $customer->status == 'trash') {
            return response()->json(['message' => 'Failed: Not found customer'], 400);
        }

        return response()->json(['json' => $customer, 'message' => 'Success'], 200);
    }

    public function searchCustomerByDni($request, $value)
    {
        $this->validate($request, [
            'dni' => 'exists:customers,dni'
        ]);

        $customer = $this->getCustomerByDni($value);

        if ($customer->status == 'I' || $customer->status == 'trash') {
            return response()->json(['message' => 'Failed: Not found customer'], 400);
        }

        return response()->json(['json' => $customer, 'message' => 'Success'], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|unique:customers,email',
            'dni' => 'required|string|unique:customers,dni',
            'name' => 'required|string',
            'last_name' => 'required|string',
            'id_com' => 'required|numeric|exists:communes,id_com',
            'address' => 'string',
        ]);

        $commune = Commune::with('region')->where('id_com', $request->id_com)->first();

        if ($commune->status === 'I' || $commune->status === 'trash') {
            return response()->json(['message' => 'Failed: Communed no found'], 400);
        }

        if ($commune->region->status == 'I' || $commune->region->status == 'trash') {
            return response()->json(['message' => 'Failed: Communed and Region no found'], 400);
        }

        Customer::create($request->all() + ['id_reg' => $commune->region->id_reg, 'date_reg' => date('Y-m-d'), 'status' => 'A']);

        return response()->json(['message' => 'Success: Customer create'], 200);
    }


    public function delete($dni)
    {
        $customer = $this->getCustomerByDni($dni);

        if ($customer->status == 'trash') {
            return response()->json(['message' => 'Failed: Not found customer'], 400);
        }

        $customer->status = 'trash';
        $customer->save();

        return response()->json(['message' => 'Success: Customer delete'], 200);
    }
}
