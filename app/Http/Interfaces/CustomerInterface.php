<?php

namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface CustomerInterface
{
    public function delete($dni);
    public function store(Request $request);
    public function getCustomerByEmail($value);
    public function getCustomerByDni($value);
    public function showByScope($scope, $value);
    public function searchCustomerByEmail($request, $value);
    public function searchCustomerByDni($request, $value);
}
