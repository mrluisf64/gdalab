<?php

namespace App\Http\Controllers;

use App\Http\Repositories\CustomerRepository;

use App\Models\Customer;

use Illuminate\Http\Request;

class CustomerController extends Controller
{

    private $repository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CustomerRepository $customer)
    {
        $this->repository = $customer;
    }

    public function show($scope, $value)
    {
        return $this->repository->showByScope($scope, $value);
    }

    public function store(Request $request)
    {
        return $this->repository->store($request);
    }

    public function destroy($dni)
    {
        return $this->repository->delete($dni);
    }

    //
}
