<?php

namespace App\Http\Controllers;

use App\Models\Region;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return Region::with('communes')->get();
    }

    //
}
