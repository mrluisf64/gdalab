<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dni', 'email', 'name', 'last_name', 'address', 'date_reg', 'status', 'id_reg', 'id_com'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id_reg', 'id_com', 'dni'
    ];

    protected $primaryKey = 'dni';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function commune()
    {
        return $this->belongsTo(Commune::class, 'id_com');
    }
}
