<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $primaryKey = 'id_com';

    public function region()
    {
        return $this->belongsTo(Region::class, 'id_reg');
    }

    public function customers()
    {
        return $this->hasMany(Costumer::class, 'dni');
    }
}
