## API REST CUSTOMERS

Hola, un placer. Realice la aplicación lo más precisa y rápida posible, utilice Lumen 8, por ende, es necesario utilizar PHP 7.3 o superior. El consumo de la API lo he resguardado en un documento de Postman para agilizar su utilización.

DOCUMENTO DE POSTMAN: https://www.getpostman.com/collections/fb262712504c5a022992

Ahora, utiliza los respectivos comandos de abajo, recordando que es necesario antes rellenar su .env con sus respectivas variables de entorno para su bd y demás.

```
composer install / update
php artisan key:generate
php artisan migrate:refresh --seed
```

Y con ello debería de bastar. Recuerde emularlo con un servidor apache (Laragon, Wamp, Lamp, etc...)

¡Gracias por esta oportunidad!
